import 'package:get/get.dart';
import 'package:get/get_instance/src/bindings_interface.dart';
import 'package:getx_code_structure/controllers/home_controller.dart';
import 'package:getx_code_structure/data/api/home_api_client/home_api_client.dart';
import 'package:getx_code_structure/data/api/home_api_client/home_api_client_impl.dart';
import 'package:getx_code_structure/data/repo/home_repo/home_repo.dart';
import 'package:getx_code_structure/data/repo/home_repo/home_repo_impl.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeApiClient>(() => HomeApiClientImpl()); // Lazy instance "IHomeProvider" created [false]
    Get.lazyPut<HomeRepo>(() => HomeRepoImpl(apiClient: Get.find())); // Lazy instance "IHomeRepository" created [false]
    Get.lazyPut(() => HomeController(homeRepository: Get.find())); // Lazy instance "HomeController" created [false]
  }
}