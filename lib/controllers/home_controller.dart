import 'package:get/get.dart';
import 'package:getx_code_structure/data/api/common/Status.dart';
import 'package:getx_code_structure/data/repo/home_repo/home_repo.dart';
import 'package:getx_code_structure/domain/cases_model.dart';

class HomeController extends GetxController with StateMixin<CasesModel> {
  HomeController({this.homeRepository});

  /// inject repo abstraction dependency
  final HomeRepo homeRepository;

  /// When the controller is initialized, make the http request
  @override
  void onInit() {
    super.onInit();
    // show loading on start, data on success
    // and error message on error with 0 boilerplate

    homeRepository.getCases().then((data) {
      if(data.status ==STATUS.SUCCESS)
      change(data.data, status: RxStatus.success());
      else
        change(null, status: RxStatus.error(data.message.toString()));
    }, onError: (err) {
      change(null, status: RxStatus.error(err.toString()));
    });



  }
}