import 'package:getx_code_structure/data/api/common/Resource.dart';
import 'package:getx_code_structure/data/api/home_api_client/home_api_client.dart';
import 'package:getx_code_structure/domain/cases_model.dart';

  abstract class HomeRepo {
   HomeRepo({this.apiClient});
  final HomeApiClient apiClient;
  Future<Resource> getCases();

}