import 'package:getx_code_structure/data/api/common/Resource.dart';
import 'package:getx_code_structure/data/api/home_api_client/home_api_client.dart';
import 'package:getx_code_structure/data/repo/home_repo/home_repo.dart';
import 'package:getx_code_structure/domain/cases_model.dart';

class HomeRepoImpl implements HomeRepo {
  HomeRepoImpl({this.apiClient});
  final HomeApiClient apiClient;

  @override
  Future<Resource> getCases() async {

    Resource cases = await apiClient.getCases("/summary");
    return cases;
  }
}