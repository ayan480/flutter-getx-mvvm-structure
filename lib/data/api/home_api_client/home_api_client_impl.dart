import 'dart:convert';

import 'package:get/get.dart';
import 'package:getx_code_structure/data/api/common/Resource.dart';
import 'package:getx_code_structure/data/api/common/Status.dart';
import 'package:getx_code_structure/domain/cases_model.dart';

import 'home_api_client.dart';



class HomeApiClientImpl extends GetConnect implements HomeApiClient {
  @override
  void onInit() {
    httpClient.defaultDecoder = CasesModel.fromJson;
    httpClient.baseUrl = 'https://api.covid19api.com';
  }

  @override
  Future<Resource> getCases(String path) async {

    // var response= get(path);
    print(path);
    final response = await get(path);
    print(response.body.toString());
    if (response.status.hasError) {
      return Resource.error(message: response.statusText);
    } else {
      if(response.statusCode == 200){
        print(response.body);
        try{
          // Map<String, dynamic> responseMap = jsonDecode(response.body);
          //   CasesModel data = CasesModel.fromJson(responseMap) ;
            return Resource(status: STATUS.SUCCESS, data: response.body);
          return Resource.success(data: response.bodyString);
        }catch(e){
          return Resource.error(message: "Something went wrong!!");
        }
      }else{
        return Resource.error(message: response.statusText);
      }
    }
  }
}