import 'package:get/get_connect/http/src/response/response.dart';
import 'package:getx_code_structure/data/api/common/Resource.dart';
import 'package:getx_code_structure/domain/cases_model.dart';

abstract class HomeApiClient {
  Future<Resource> getCases(String path);
}